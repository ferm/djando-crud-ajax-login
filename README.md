# Django con AJAX

Es un proyecto de ejemplo que desarrollé, para el apredizaje inicial del frameworks django. Usando nociones básicas de altas, bajas, consultas y modificaciones de registros en bases de datos. A continuación listaré los requerimientos y pasos para levantar éste proyecto en Fedora:

![Login.](media/main.png)

### Requeremientos
- Fedora 34 (no probado en otro SO)
- Python 3.9
- Django 3.2.3
- mariadb  10.4
- bootstrap 5.0.1

### Instalación

- Instalar dependencias requeridas para fedora

	`sudo dnf install python3-devel mysql-devel python3-virtualenv`


	``
	export MYSQLCLIENT_CFLAGS=`pkg-config mysqlclient --cflags`
	``
	
	``
	export MYSQLCLIENT_LDFLAGS=`pkg-config mysqlclient --libs`
	``

- Clonar el respositorio

	`
	git clone git@gitlab.com:ferm/djando-crud-ajax-login.git
	`

- En éste proyecto se usó mariadb, instalado mediante XAMPP, si no lo tienen instalado, pueden seguir los pasos de éste tutorial https://blog.linuxitos.com/post/instalar-xampp-8-0-3-fedora-34 

- Un vez instalado XAMPP, es necesario crear una base de datos con el nombre de django_app1, es el nombre que se usa en el proyecto, pueden modificarlo, pero también deberán modificar el nombre en el archivo settings.py

- Entrar a la carpeta del repositorio

- Crear un entorno virtualenv

	`
	virtualenv -p python3 .
	`

- Activar el entorno de python

	`
		source bin/activate
	`

- Instalar requerimientos de python

	`
		pip3 install -r requirements.txt
	`

- Iniciar migración de modelos y generar migraciones

	Antes de iniciar con éste comando, es necesario configurar el archivo settings.py, y establecer el nombre de la base de datos, el nombre de usuario, el puerto, y la contraseña del servidor de bases de datos.

	`
	python manage.py makemigrations 
	`

- Migrar las tablas a base de datos


	`
	python manage.py migrate
	`

- Iniciar el servidor
	
	`
		python manage.py runserver
	`
	
	
	
	
	
	
	
	
	
	
	
	
	
	