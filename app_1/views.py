from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import redirect, render
from django.http import JsonResponse
from .forms import ContactForm, AddPostForm
from django.views import View
from .models import Posts, Profile
import math
from .utils import paginate
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserUpdateForm, ProfileUpdateForm, UserRegisterForm
# Create your views here.


def index(request):
	posts = Posts.objects.all().values("id", "nom_post", "fc_post", 'slug_post', 'user_id', 'img_post').order_by("-id")
	return render(request, "main.html", {"posts": posts})

#FBV
def contactPage(request):
	form = ContactForm()
	return render(request, "contact.html", {"contactForm": form})


def postContact(request):
	if request.method == "POST" and request.is_ajax():
		form = ContactForm(request.POST)
		form.save()
		return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Post agregado", "success": True}, status=200)
	return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, intenta más tarde.", "success": False}, status=400)


def addPost(request):
	if request.user.is_authenticated:
		form = AddPostForm()
		return render(request, "addPost.html", {"form": form})
	return redirect('login')



def ajxAddPost(request):
	if request.user.is_authenticated:
		if request.method == "POST" and request.is_ajax():
			form = AddPostForm(request.POST, request.FILES)
			if form.is_valid():
				form.save()
				return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Registro agregado correctamente", "success": True}, status=200)
			else:
				return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error en la validación del formulario.", "success": False}, status=400)
		else:
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, intenta más tarde.", "success": False}, status=400)
	else:
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Por favor inicia sesión.", "success": False}, status=400)



def viewPost(request, slug):
	if request.method == "GET":
		try:
			post = Posts.objects.get(slug_post=slug)
			exist = True
		except:
			post = False
			exist = False
	else:
		post = False
		exist = False
	return render(request, "viewPost.html", {"post": post, "exist": exist})



#CBV
class ContactAjax(View):
	form_class = ContactForm
	template_name = "contact_cbv.html"

	def get(self, *args, **kwargs):
		form = self.form_class()
		return render(self.request, self.template_name, {"contactForm": form})

	def post(self, *args, **kwargs):
		if self.request.method == "POST" and self.request.is_ajax():
			form = self.form_class(self.request.POST)
			form.save()
			return JsonResponse({"tipo":"success","icon":"bi bi-check-circle", "msg":"Registro agregado correctamente","success":True}, status=200)
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, intenta más tarde.", "success": False}, status=400)


def userPosts(request):
	if request.user.is_authenticated:
		posts = Posts.objects.all().values("id", "nom_post", "fc_post", 'slug_post', 'user')
		return render(request, "userPosts.html", {"posts": posts})
	return redirect('login')



def loadPosts(request):
	user_id = None
	if request.user.is_authenticated:
		user_id = request.user.id
		if request.method == "POST" and request.is_ajax():
			record 		= int(request.POST.get('page'))
			edo 		= int(request.POST.get('filter'))
			order_by 		= request.POST.get('order_by')
			order = request.POST.get('order')
			page 		= record if record>0 else 1
			per_page 	= int(request.POST.get('limite'))
			offset 		= (page - 1) * per_page
			search 		= request.POST.get('search')
			total 		= 0;
			if edo==2 :
				total = Posts.objects.filter(
					nom_post__icontains=search, user=user_id).count()
				posts = Posts.objects.filter(nom_post__icontains=search, user=user_id).order_by(
					order+order_by).all()[offset:offset+per_page]
			else:
				total = Posts.objects.filter(
					act_post=edo, nom_post__icontains=search, user=user_id).count()
				posts = Posts.objects.filter(act_post=edo, nom_post__icontains=search, user=user_id).order_by(
					order+order_by).all()[offset:offset+per_page]
			
			if total > 0:
				total_pages = math.ceil(total/per_page)
				adyacentes 	= 2
				data = ""
				data += '<div class="table-responsive">'
				data += '<table class="table table-hover table-fixed">'
				data += '<thead>'
				data += '<tr class="row-link">'
				data += '<th class="text-left w-10">'
				data += '<div class="checkbox checkbox-danger" style="display: inline;">'
				data += '<input id="chk-all-regs" type="checkbox">'
				data += '<label for="chk-all-regs" style="padding-bottom: 15px;"> </label>'
				data += '</div>'
				data += '<button class="btn btn-sm quick-btn btn-link text-danger mdl-del-regs" id="btn-del-list" title="Eliminar seleccionados" data-bs-toggle="modal" data-bs-target="#mdl-del-regs" disabled="disabled">'
				data += '<i class="bi bi-trash"> </i>'
				data += '<i id="spn-del" class="spn-total"> </i>'
				data += '</button>'
				data += '</th>'
				data += '<th data-field="id" class="th-link text-left"> <i class="fas fa-sort"> </i>  # </th>'
				data += '<th data-field="nom_post" class="th-link"> <i class="fas fa-sort"> </i> Nombre </th>'
				data += '<th class = "text-center w-10" > Acciones </th>'
				data += '</tr>'
				data += '</thead>'
				data += '<tbody>'

				for post in posts:
					data += '<tr id="row-id-'+str(post.id)+'">'
					data += '<td class="text-left">'
					data += '<div class="checkbox checkbox-primary">'
					data += '<input type="checkbox" class="chks-regs" name="chk-reg-'+str(post.id)+'" id="chk-reg-'+str(post.id)+'" data-iddel="'+str(post.id)+'">'
					data += '<label for="chk-reg-'+str(post.id)+'"> </label>'
					data += '</div>'
					data += '</td>'
					data += '<td class="text-left">'+str(post.id)+'</td>'
					data += '<td>'
					data += '<a href="view/'+post.slug_post+'"> '+post.nom_post+'</a></td>'
					data += '<td class="text-center">'
					data += '<a href="edit/'+str(post.id)+'" class="btn btn-primary btn-sm"><i class="bi bi-pencil-square"></i></a> '
					data += ' <button type="button" class="btn btn-danger mdl-del-reg btn-sm" title="Eliminar" data-bs-toggle="modal" data-bs-target="#mdl-del-reg" data-idreg="'+str(post.id)+'" data-nomreg="'+post.nom_post+'">'
					data += '<i class="bi bi-trash"> </i></button></td></tr>'

				data += '</tbody></table></div>'
				return JsonResponse({"total": total, "data": data, "paginate": paginate(page, total_pages, adyacentes, "load"), "success": True}, status=200)
			else:
				return JsonResponse({"total": total, "data": '<div class="text-center alert alert-info" role="alert"><i class="bi bi-info-circle"></i> Sin resultados.</div>', "paginate": "", "success": True}, status=200)
		else:
			return JsonResponse({"total": "0", "data": '<div class="text-center alert alert-danger" role="alert"><i class="bi bi-x-circle"></i> Error interno, intenta más tarde.</div>', "success": False}, status=400)
	else:
		return JsonResponse({"total": "0", "data": '<div class="text-center alert alert-danger" role="alert"><i class="bi bi-x-circle"></i> Por favor inicia sesión.</div>', "success": False}, status=400)



def loadscroll(request):
	if request.method == "POST" and request.is_ajax():
		order_by = request.POST.get('order_by')
		order = request.POST.get('order')
		search = request.POST.get('search')
		start = int(request.POST.get('start'))
		limit = int(request.POST.get('limit'))

		total = 0
		total = Posts.objects.filter(act_post=1, nom_post__icontains=search).count()
		posts = Posts.objects.filter(act_post=1, nom_post__icontains=search).order_by(order+order_by).all()[start:start+limit]

		if total > 0:
			data = ""
			for post in posts:
				formatedDate = post.fc_post.strftime("%b %d, %Y")
				img = '/media/' + \
					str(post.img_post) if post.img_post else 'static/img/post.png'
				data += '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 mb-2 d-flex">'
				data += '<div class="card mb-2 post border-bottom-2" style="width:100%;">'
				data += '<a href="view/'+post.slug_post+'">'
				data += '<div class="post-image rounded">'
				data += '<img src="'+str(img)+'" class="" alt="" sizes="(max-width: 272px) 100vw, 272px">'
				data += '</div>'
				data += '</a>'
				data += '<div class="card-body font-weight-bold ">'
				data += '<a class="text-dark" href="view/'+post.slug_post+'">'+post.nom_post+'</a><div>'
				data += '<small class="float float-right"> <span class="byline-author-label"> By </span>'
				data += '<a class="byline-author-name-link" href="#" title="LiNuXiToS"> '+post.user.username+' </a> </small>'
				data += '<small class="float-end"> '+(str(formatedDate))+' </small></div></div></div></div>'
			return JsonResponse({"total": total, "data": data, "search":search, "success": True}, status=200)
		else:
			return JsonResponse({"total": total,"search":search, "data": '<div class="col-md-12"><div class="text-center alert alert-info" role="alert"><i class="bi bi-info-circle"></i> Sin resultados.</div></div>', "paginate": "", "success": True}, status=200)
	else:
		return JsonResponse({"total": "0", "search": "", "data": '<div class="col-md-12"><div class="text-center alert alert-danger" role="alert"><i class="bi bi-x-circle"></i> Error interno, intenta más tarde.</div></div>', "success": False}, status=400)



def deletePost(request):
	if request.user.is_authenticated:
		if request.method == "POST" and request.is_ajax():
			ids = request.POST.get('list_ids').split('|')
			for id in ids:
				act_post = int(request.POST.get('act_post'))
				reg = Posts(id=id, act_post=act_post)
				affected = Posts.objects.filter(id=id).update(act_post=act_post)
				if affected > 0 :
					return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Registro actualizado.", "success": True}, status=200)
				else:
					return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Registro actualizado.", "success": True}, status=200)
		else:
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, intenta más tarde.", "success": False}, status=400)
	return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, intenta más tarde.", "success": False}, status=400)



def editPost(request, id):
	if request.user.is_authenticated:
		if request.method == "GET":
			#id = request.GET.get("id")
			if id > 0:
				try:
					post = Posts.objects.get(id=id, user_id=request.user.id)
					exist = True
				except:
					post = False
					exist = False
			else:
				post = False
				exist = False
		
		if request.method == "POST":
			act_post = request.POST.get("act_post")
			nom_post = request.POST.get("nom_post")
			desc_post = request.POST.get("desc_post")
			#affected = Posts.objects.filter(id=id).update(act_post=act_post, nom_post=nom_post, desc_post=desc_post)
			data = Posts.objects.get(id=id)
			form = AddPostForm(request.POST, request.FILES, instance = data)
			if form.is_valid():
				form.save()
				update = True
				msg = "Actualización correcta"
				post = Posts.objects.get(id=id)
				exist = True
			else:
				post = False
				exist = False
				update = False
				msg = "Error en la actualización"

		return render(request, "editPost.html", {"post": post, "exist":exist})
	return redirect('login')



#login qith ajax
def loginUser(request):
	username = request.POST.get('username')
	password = request.POST.get('password')
	# stayloggedin = request.GET.get('stayloggedin')
	# if stayloggedin == "true":
	#  pass
	# else:
	#  request.session.set_expiry(0)
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			try:
				profile = Profile.objects.get(user=user)
			except Profile.DoesNotExist:
				obj = Profile.objects.create(user=user)
				obj.save()

			login(request, user)
			return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Inicio de sesión correcto.", "success": True}, status=200)
		else:
			return JsonResponse({"tipo": "info", "icon": "bi bi-info-circle", "msg": "Usuario inactivo, por favor intente más tarde.", "success": True}, status=200)
	else:
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Usuario o contraseña incorrectos.", "success": True}, status=200)



class signup(generic.CreateView):
	form_class = UserCreationForm
	success_url = reverse_lazy('login')
	template_name = 'registration/signup.html'



def old_ajax_registerUser(request):
	#if request.user.is_authenticated:
	#	return redirect('posts')
	if request.method == 'POST' and request.is_ajax():
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			login(request, user)
			#return redirect('posts')
			return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Usuario registrado.", "success": True}, status=200)
		else:
			#return render(request, 'registration/signup.html', {'form': form})
			msg = ""
			for error in form.errors:
				msg += form.errors[error]
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": msg, "success": True}, status=200)

	else:
		form = UserCreationForm()
		#return render(request, 'registration/signup.html', {'form': form})
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Registro actualizado.", "success": True}, status=200)



def registerUser(request):
	if request.method == 'POST':
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			login(request, user)
			#username = form.cleaned_data.get('username')
			#messages.success(request, f'Your account has been created! You are now able to log in')
			#return redirect('login')
			return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Usuario registrado.", "success": True}, status=200)
		else:
			#return render(request, 'registration/signup.html', {'form': form})
			msg = ""
			for error in form.errors:
				msg += error+form.errors[error]
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": msg, "success": True}, status=200)
	else:
		#form = UserRegisterForm()
		#return render(request, 'users/register.html', {'form': form})
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Registro actualizado.", "success": True}, status=200)



def search(request):
	search = request.GET.get('q')
	posts = Posts.objects.all().values("id", "nom_post", "fc_post", 'slug_post',
									'user_id', 'img_post').order_by("-id")
	return render(request, "search.html", {"posts": posts, "search": search})


def profile(request):
	if request.user.is_authenticated:
		try:
			p_form = request.user.profile
		except Profile.DoesNotExist:
			p_form = Profile(user=request.user)
		
		id = request.user.id
		#return render(request, "profile.html")
		u_form = UserUpdateForm(instance=request.user)
		p_form = ProfileUpdateForm(instance=request.user.profile)
		context = {
			'u_form': u_form,
			'p_form': p_form
		}
		return render(request, 'profile.html', context)
	else:
		return redirect('login')


def supprofile(request):
	if request.user.is_authenticated:
		id = request.user.id
		if request.method == "POST" and request.is_ajax():
			data = User.objects.get(id=id)
			#form = AddPostForm(request.POST, request.FILES, instance=data)
			#form = UserForm(request.POST)
			#form = UserCreationForm(request.POST, instance=data)
			if data:
				#user = form.save(commit=False)
				#form.save()
				return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Actualización correcta."+str(id)+'-'+data.username, "success": True}, status=200)
			else:
				return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error en el formulario."+str(id), "success": True}, status=200)
		else:
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Por favor reinicia sesión.", "success": True}, status=200)
	else:
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Por favor inicia sesión.", "success": True}, status=200)
	#return redirect('login')


@login_required
def upprofile(request):
	if request.method == 'POST' and request.is_ajax():
		user = request.user
		u_form = UserUpdateForm(request.POST, instance=request.user)
		#u_form = ProfileUpdateForm(request.POST or None, request.FILES, instance=user.profile)
		try:
			#p_form = request.user.profile
			p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
		except Profile.DoesNotExist:
			p_form = Profile(user=request.user)
			#p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.profile)

		if u_form.is_valid() and p_form.is_valid():
		#if u_form.is_valid():
			u_form.save()
			p_form.save()
			#messages.success(request, f'Your account has been updated!')
			#return redirect('profile')
			return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Actualización correcta.", "success": True}, status=200)
	else:
		u_form = UserUpdateForm(instance=request.user)
		#p_form = ProfileUpdateForm(instance=request.user.profile)
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, reinicia sesión.", "success": True}, status=200)


@login_required
def bk_upprofile(request):
	user = request.user
	#form = ProfileUpdateForm(request.POST or None, request.FILES, instance=user.profile)
	form = ProfileUpdateForm(request.POST or None, initial={'image': user.profile.image})
	if request.method == 'POST' and request.is_ajax():
		if form.is_valid():
			form.save()
			#print("POST sending you back HOME")
			#return HttpResponseRedirect(reverse('home'))
			return JsonResponse({"tipo": "success", "icon": "bi bi-check-circle", "msg": "Actualización correcta.", "success": True}, status=200)
		else:
			#print("error")
			return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno."+form.errors, "success": True}, status=200)
	else:
		#print("GET profile.html")
		context = {
			"profile_updateform": form,
		}
		#return render(request, 'profile.html', context)
		return JsonResponse({"tipo": "danger", "icon": "bi bi-x-circle", "msg": "Error interno, no es método post por ajax.", "success": True}, status=200)
