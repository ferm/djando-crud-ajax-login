from django.db import models
from django.utils.text import slugify
from .utils import random_string_generator
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from PIL import Image
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.
class Contact(models.Model):
	id 			= models.BigAutoField(primary_key=True)
	name 		= models.CharField(max_length = 100)
	email 		= models.EmailField()
	message 	= models.TextField()
	timestamp 	= models.DateTimeField(auto_now_add = True)
	def __str__(self):
		return self.name



def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]



class Posts(models.Model):
	id 			= models.BigAutoField(primary_key=True)
	fc_post 	= models.DateTimeField(auto_now_add=True, null=True)
	act_post 	= models.IntegerField(default=1)
	nom_post 	= models.CharField(max_length=200)
	desc_post 	= models.TextField()
	img_post 	= models.ImageField(upload_to='images/', default="")
	slug_post 	= models.SlugField(max_length=200, unique=True, editable=False)
	#user 		= models.OneToOneField(User, on_delete=models.CASCADE)
	user 		= models.ForeignKey(User, null=True, related_name='user',  on_delete=models.SET(get_sentinel_user),)  # models.IntegerField()

	def save(self, *args, **kwargs):
		new_slug 		= "{slug}-{randstr}".format(slug=" ".join(self.nom_post.split()[:4]), randstr=random_string_generator(size=4))
		self.slug_post 	= slugify(new_slug, allow_unicode=False)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.nom_post



"""class UserProfile(models.Model):
	profile_user = models.OneToOneField(User, on_delete=models.CASCADE)
	profile_img = models.ImageField(default='default.png', upload_to='profile_pics')"""

class Profile(models.Model):
	id 		= models.BigAutoField(primary_key=True)
	image = models.ImageField(default='default.svg', upload_to='profile_pics')
	user 	= models.OneToOneField(User, on_delete=models.CASCADE)
	
	"""def __str__(self):
		return f'{self.user.username} Profile'"""

	"""def save(self):
		super().save()
		img = Image.open(self.image.path)
		if img.height > 300 or img.width > 300:
			output_size = (300, 300)
			img.thumbnail(output_size)
			img.save(self.image.path)"""


@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance)
	instance.profile.save()



"""class bk_Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	#portfolio_site = models.URLField(blank=True)
	image = models.ImageField(upload_to='profile_pics', blank=True, default='default.jpg')

	def __str__(self):
		return self.user.username"""
