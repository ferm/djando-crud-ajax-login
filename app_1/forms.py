from django import forms
from .models import Contact, Posts
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

class ContactForm(forms.ModelForm):
	class Meta:
		model = Contact
		exclude = ["timestamp", ]
		widgets = {
			'message': forms.Textarea(attrs={'rows':4, 'cols':15}),
		}

	def __init__(self, *args, **kwargs):
		super(ContactForm, self).__init__(*args, **kwargs)
		for field in self.fields:
			self.fields[field].widget.attrs.update({'class': 'form-control'})



class AddPostForm(forms.ModelForm):
	class Meta:
		model = Posts
		fields = ["act_post", "nom_post", "desc_post", "img_post", "user"]
		exclude = ["timestamp", ]
		widgets = {
			'desc_post': forms.Textarea(attrs={'rows': 4, 'cols': 15}),
		}

	def __init__(self, *args, **kwargs):
		super(AddPostForm, self).__init__(*args, **kwargs)
		for field in self.fields:
			self.fields[field].widget.attrs.update({'class': 'form-control'})



class UserRegisterForm(UserCreationForm):
	#email = forms.EmailField()
	class Meta:
		model = User
		fields = ['username', 'password1']



class UserUpdateForm(forms.ModelForm):
	email = forms.EmailField()
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email']



class ProfileUpdateForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = ['image']


"""class ProfileUpdateForm(forms.ModelForm):
	class Meta():
		model = Profile
		fields = ["image"]

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		#self.fields["portfolio_site"].label = "Site"
		self.fields["image"].label = "Image"""
